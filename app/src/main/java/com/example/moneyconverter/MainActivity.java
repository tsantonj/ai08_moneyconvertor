package com.example.moneyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private final Map<String, Double> moneyMap = new HashMap<String, Double>();
    private EditText currencyTop;
    private EditText currencyBottom;
    private Spinner spinnerCurrencyTop;
    private Spinner spinnerCurrencyBottom;
    private Button exchange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMoneyMap();
        initComponents();
    }

    private void initMoneyMap() {
        moneyMap.put("EUR", 1.0);
        moneyMap.put("USD", 1.1);
        moneyMap.put("GBP", 0.90);
    }

    private void initComponents() {
        // Get field reference
        currencyTop = findViewById(R.id.currencyTop);
        currencyBottom = findViewById(R.id.currencyBottom);
        spinnerCurrencyTop = findViewById(R.id.spinnerCurrencyTop);
        spinnerCurrencyBottom = findViewById(R.id.spinnerCurrencyBottom);

        // Initialize spinner values
        String currencies[] = {"EUR", "USD", "GBP"};
        ArrayAdapter<String> currenciesAdaptater = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, currencies);
        spinnerCurrencyTop.setAdapter(currenciesAdaptater);
        spinnerCurrencyBottom.setAdapter(currenciesAdaptater);

        // Start listeners
        currencyTopListener();
        spinnerCurrencyTopListener();
        spinnerCurrencyBottomListener();
    }

    private String currencyConvertion(String inputCurrency, Double convertionFactor) {
        Double currency = Double.parseDouble(inputCurrency);
        currency = currency * convertionFactor;

        // convertion dégueux
        currency = (double) ((int) (currency.doubleValue() * 100)) /100;

        return String.valueOf(currency);
    }

    private Double getConversionFactor() {
        Double conversionFactor = 0.0;
        String currencyTop = spinnerCurrencyTop.getSelectedItem().toString();
        String currencyBottom = spinnerCurrencyBottom.getSelectedItem().toString();

        conversionFactor = moneyMap.get(currencyBottom) / moneyMap.get(currencyTop);

        return conversionFactor;
    }

    private void setBottomTextBoxValue() {
        Double conversionFactor = getConversionFactor();
        String convertedCurrency = currencyConvertion(currencyTop.getText().toString(), conversionFactor);
        currencyBottom.setText(convertedCurrency);
    }

    private void currencyTopListener() {
        currencyTop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!currencyTop.getText().toString().isEmpty())
                    setBottomTextBoxValue();
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    private void spinnerCurrencyTopListener() {
        spinnerCurrencyTop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!currencyTop.getText().toString().isEmpty())
                    setBottomTextBoxValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {  }
        });
    }

    private void spinnerCurrencyBottomListener() {
        spinnerCurrencyBottom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!currencyTop.getText().toString().isEmpty())
                    setBottomTextBoxValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    public void onClickExchange(View view) {
        String top = currencyTop.getText().toString();
        currencyTop.setText(currencyBottom.getText().toString());
        currencyBottom.setText(top);

        int topSpinner = (int) spinnerCurrencyTop.getSelectedItemId();
        spinnerCurrencyTop.setSelection((int) spinnerCurrencyBottom.getSelectedItemId());
        spinnerCurrencyBottom.setSelection(topSpinner);
    }
}
